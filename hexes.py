"""Hex grid coordinate operations."""
import enum
from collections import deque


# pre-calculate constants to avoid doing math inline
_sqrt3 = 3 ** 0.5
_sqrt3_2 = _sqrt3 / 2
_sqrt3_3 = _sqrt3 / 3
_3_2 = 3 / 2
_1_3 = 1 / 3
_2_3 = 2 / 3


class HexShape(enum.Enum):
    """Enum to handle the shape (aka orientation) of a hex."""

    same = enum.auto()
    flat = enum.auto()
    pointy = enum.auto()

    @staticmethod
    def parse(text):
        """Parse a string into the enum."""
        text = text.lower()
        if text == 'same':
            return HexShape.same
        elif text == 'flat':
            return HexShape.flat
        elif text == 'pointy':
            return HexShape.pointy
        raise Exception()

    def __invert__(self):
        """Convert to the other shape."""
        if self == HexShape.flat:
            return HexShape.pointy
        elif self == HexShape.pointy:
            return HexShape.flat
        else:
            raise Exception('Only flat and pointy may be negated.')


# https://www.redblobgames.com/grids/hexagons
def cube_round(q_frac, r_frac, s_frac):
    """Round a fractional hex-cube coordinate."""
    q = round(q_frac)
    r = round(r_frac)
    s = round(s_frac)

    q_diff = abs(q - q_frac)
    r_diff = abs(r - r_frac)
    s_diff = abs(s - s_frac)

    if q_diff > r_diff and q_diff > s_diff:
        q = -(r + s)
    elif r_diff > s_diff:
        r = -(q + s)
    else:
        s = -(q + r)

    return q, r, s


def axial_round(q, r):
    """Round a fractional hex-axial coordinate."""
    q, r, s = cube_round(q, r, -(q + r))
    return q, r


def pixel_to_hex_e(x, y, size, shape):
    """Find the hex that contains the pixel."""
    if shape == HexShape.flat:
        q = ( _2_3 * x                 ) / size  # noqa: E221, E222
        r = (-_1_3 * x  +  _sqrt3_3 * y) / size  # noqa: E221, E222
        return axial_round(q, r)

    elif shape == HexShape.pointy:
        q = (_sqrt3_3 * x  -  _1_3 * y) / size  # noqa: E221, E222
        r = (                 _2_3 * y) / size  # noqa: E221, E222
        return axial_round(q, r)

    else:
        raise Exception()


def hex_to_pixel_e(q, r, size, shape):
    """Find the central pixel of the hex."""
    if shape == HexShape.flat:
        x = size * (    _3_2 * q               )  # noqa: E221, E222
        y = size * (_sqrt3_2 * q  +  _sqrt3 * r)  # noqa: E221, E222
        return x, y

    elif shape == HexShape.pointy:
        x = size * (_sqrt3 * q  +  _sqrt3_2 * r)  # noqa: E221, E222
        y = size * (                   _3_2 * r)  # noqa: E221, E222
        return x, y

    else:
        raise Exception()


def make_hex_e(size, shape):
    """Get the points of the hex centered on the origin."""
    if shape == HexShape.flat:
        h = _sqrt3 * size
        w = 2 * size
        return (
            ( w / 2,      0),
            ( w / 4, -h / 2),
            (-w / 4, -h / 2),
            (-w / 2,      0),
            (-w / 4,  h / 2),
            ( w / 4,  h / 2))

    elif shape == HexShape.pointy:
        h = 2 * size
        w = _sqrt3 * size
        return (
            ( w / 2, -h / 4),
            (     0, -h / 2),
            (-w / 2, -h / 4),
            (-w / 2,  h / 4),
            (     0,  h / 2),
            ( w / 2,  h / 4))

    else:
        raise Exception()


def rotate(q, r):
    """Rotate axial coordinate (q, r) about (0, 0)."""
    s = -q + -r
    q, r, s = -r, -s, -q
    return q, r


def iter_mismatched_edge_hexes(radius):
    """Iterate the minor hexes on the edge of a hex with the opposite shape."""
    if radius < 1:
        yield 0, 0
        return

    qd, rd = 1, 0
    q, r = 0, -(radius)
    for _ in range(radius):

        for _ in range(6):
            yield q, r
            q, r = rotate(q, r)

        q += qd
        r += rd


def iter_matched_edge_hexes(radius):
    """Iterate the minor hexes on the edge of a hex with the same shape."""
    if radius <= 1:
        return

    k = (radius + 1) // 3
    max_n = 2 * k - 1

    qd, rd = 2, -1
    q0, r0 = -1 - (k - 1) * qd, -(radius - 1) - (k - 1) * rd

    for n in range(max_n + 1):
        q, r = q0 + n * qd, r0 + n * rd
        yield q, r
        for _ in range(5):
            q, r = rotate(q, r)
            yield q, r


def iter_neighbors(q, r):
    """Iterate the neighboring hexes."""
    q0, r0 = 0, -1
    yield q + q0, r + r0
    for _ in range(5):
        q0, r0 = rotate(q0, r0)
        yield q + q0, r + r0


def point_in_box(x, y, box):
    """Test if a point is contained within a box."""
    return box[0] <= x <= box[2] and box[1] <= y <= box[3]


def boxes_overlap(box1, box2, flip=True):
    """Test whether two boxes overlap."""
    return (
        point_in_box(box1[0], box1[1], box2)
        or point_in_box(box1[0], box1[3], box2)
        or point_in_box(box1[2], box1[1], box2)
        or point_in_box(box1[2], box1[3], box2)
        or box1[0] <= box2[0] <= box2[2] <= box1[2]
            and box2[1] <= box1[1] <= box1[3] <= box2[3]  # noqa: E131
        or flip and boxes_overlap(box2, box1, False))


def grid(grid_box, hex_box, f_hex, f_pixel):
    """Generate the hexes that are contained within a box."""
    q, r = f_hex(grid_box[0], grid_box[1])
    stack = deque()
    stack.append((q, r))
    grid = set()
    grid.add(stack[0])

    while stack:
        q, r = stack.pop()
        for q, r in iter_neighbors(q, r):
            if (q, r) in grid:
                continue

            x, y = f_pixel(q, r)
            box = (
                hex_box[0] + x, hex_box[1] + y,
                hex_box[2] + x, hex_box[3] + y)

            if boxes_overlap(box, grid_box):
                stack.append((q, r))
                grid.add((q, r))

    return sorted(grid)


def sign(x):
    """Return the sign of a number."""
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0


_mismatched_directions = {
    ( 0,  0,  0): tuple(),
    ( 0, -1,  0): ((+1, -1), ),
    (+1, -1,  0): ((+1, -1), (+1,  0)),
    (+1,  0,  0): ((+1,  0), ),
    (+1,  0, -1): ((+1,  0), ( 0, +1)),
    ( 0,  0, -1): (( 0, +1), ),
    ( 0, +1, -1): (( 0, +1), (-1, +1)),
    ( 0, +1,  0): ((-1, +1), ),
    (-1, +1,  0): ((-1, +1), (-1,  0)),
    (-1,  0,  0): ((-1,  0), ),
    (-1,  0, +1): ((-1,  0), ( 0, -1)),
    ( 0,  0, +1): (( 0, -1), ),
    ( 0, -1, +1): (( 0, -1), (+1, -1)) }


def mismatched_direction(q, r):
    """Return the direction of a hex from the origin for mismatched grids."""
    s = -q + -r
    v = q, r, s
    n = max(abs(x) for x in v)
    d = tuple(sign(x) if abs(x) == n else 0 for x in v)
    return _mismatched_directions[d]


_matched_directions = {
    ( 0,  0,  0): tuple(),
    ( 0, -1,  0): (( 0, -1), ),
    (+1, -1,  0): (( 0, -1), (+1, -1)),
    (+1,  0,  0): ((+1, -1), ),
    (+1,  0, -1): ((+1, -1), (+1,  0)),
    ( 0,  0, -1): ((+1,  0), ),
    ( 0, +1, -1): ((+1,  0), ( 0, +1)),
    ( 0, +1,  0): (( 0, +1), ),
    (-1, +1,  0): (( 0, +1), (-1, +1)),
    (-1,  0,  0): ((-1, +1), ),
    (-1,  0, +1): ((-1, +1), (-1,  0)),
    ( 0,  0, +1): ((-1,  0), ),
    ( 0, -1, +1): ((-1,  0), ( 0, -1)) }


def matched_direction(q, r):
    """Return the direction of a hex from the origin for matched grids."""
    s = -q + -r
    v = q - r, r - s, s - q
    n = max(abs(x) for x in v)
    d = tuple(sign(x) if abs(x) == n else 0 for x in v)
    return _matched_directions[d]


def draw_polygon(draw, xy, fill=None, outline=None, width=1):
    """
    Reimplementation of PIL.ImgeDraw.Draw.polygon.

    This will draw lines so that they grow in width from the center of the
    line, instead of growing inward toward the center of the polygon, since the
    latter would double the apparent thickness of lines for shapes that are
    tiled next to each other.
    """
    if fill is not None:
        draw.polygon(xy, fill=fill, outline=None)

    # duplicate the first point to close the polygon. handle both legal cases
    # where xy is a list of two-tuples and when it is a list of interleaved
    # coordinates. don't check too hard, let draw.line handle the rest.
    try:
        xy = (*xy, (xy[0][0], xy[0][1]))
    except Exception:
        # if this fails, xy wasn't valid
        xy = (*xy, xy[0], xy[1])

    draw.line(xy, fill=outline, width=width)


class MajorMinorHexGrid:
    """
    Class defining relationships between major and minor hexes.

    The two types of hexes are related to each other based on several
    attributes:

    - An origin coordinate that is the center of the grid, minor hex (0, 0) and
    major hex (0, 0);
    - The length of the sides of the minor hexes;
    - The radius of the major hex in terms of minor hexes;
    - The shape or orientation of the minor and major hexes.
    """

    def __init__(self, x, y, minor_size, radius, major_shape, minor_shape):
        """
        Construct the object.

        x, y - The origin pixel which all coordinates are relative to.
        minor_size - The length of the sides of minor hexes in pixels.
        radius - The radius of major hexes in terms of minor hexes.
        major_shape - The orientation of major hexes, flat or pointy.
        minor_shape - The orientation of minor hexes, flat or pointy.
        """
        self.x = x
        self.y = y
        self.minor_size = minor_size
        self.radius = radius
        self.major_shape = major_shape
        self.minor_shape = minor_shape

        if major_shape == minor_shape:
            self.major_size = (2 * radius - 1) * minor_size
            self.edge = {
                (q, r): matched_direction(q, r)
                for q, r in iter_matched_edge_hexes(radius)}
        else:
            self.major_size = _sqrt3 * minor_size * radius
            self.edge = {
                (q, r): mismatched_direction(q, r)
                for q, r in iter_mismatched_edge_hexes(radius)}

        self.major_polygon = make_hex_e(self.major_size, major_shape)
        self.minor_polygon = make_hex_e(minor_size, minor_shape)

        self.major_box = (
            min(x for x, y in self.major_polygon),
            min(y for x, y in self.major_polygon),
            max(x for x, y in self.major_polygon),
            max(y for x, y in self.major_polygon))
        self.minor_box = (
            min(x for x, y in self.minor_polygon),
            min(y for x, y in self.minor_polygon),
            max(x for x, y in self.minor_polygon),
            max(y for x, y in self.minor_polygon))

    def major_hex(self, x, y):
        """Return the axial coordinates of major hex that contains a pixel."""
        return pixel_to_hex_e(
            x - self.x, y - self.y, self.major_size, self.major_shape)

    def minor_hex(self, x, y):
        """Return the axial coordinates of minor hex that contains a pixel."""
        return pixel_to_hex_e(
            x - self.x, y - self.y, self.minor_size, self.minor_shape)

    def major_pixel(self, q, r):
        """Return the pixel at the center of a major hex."""
        x, y = hex_to_pixel_e(q, r, self.major_size, self.major_shape)
        return self.x + x, self.y + y

    def minor_pixel(self, q, r):
        """Return the pixel at the center of a minor hex."""
        x, y = hex_to_pixel_e(q, r, self.minor_size, self.minor_shape)
        return self.x + x, self.y + y

    def major_grid(self, box):
        """Return the major hexes contained in a box."""
        return grid(box, self.major_box, self.major_hex, self.major_pixel)

    def minor_grid(self, box):
        """Return the minor hexes contained in a box."""
        return grid(box, self.minor_box, self.minor_hex, self.minor_pixel)

    def draw_major(self, draw, q, r, smoothing=None, **kwargs):
        """Draw a major hex."""
        x0, y0 = self.major_pixel(q, r)
        line = ((x0 + x, y0 + y) for x, y in self.major_polygon)

        if smoothing:
            line = (smoothing(x, y) for x, y in line)

        draw_polygon(draw, [*line], **kwargs)

    def draw_minor(self, draw, q, r, smoothing=None, **kwargs):
        """Draw a minor hex."""
        x0, y0 = self.minor_pixel(q, r)
        line = ((x0 + x, y0 + y) for x, y in self.minor_polygon)

        if smoothing:
            line = (smoothing(x, y) for x, y in line)

        draw_polygon(draw, [*line], **kwargs)

    def iter_major_hexes(self, q, r):
        """Iterate the major hexes that a minor hex belongs to."""
        # find a major hex that the minor hex belongs to by finding the minor
        # hex's central coordinate and then finding a major hex that contains
        # that coordinate
        x, y = self.minor_pixel(q, r)
        q0, r0 = self.major_hex(x, y)

        # yield the primary major hex
        yield q0, r0

        # normalize the minor hex to be in the origin major hex
        x0, y0 = self.major_pixel(q0, r0)
        q1, r1 = self.minor_hex(x - x0 + self.x, y - y0 + self.y)

        # now look to see if the minor hex is on the edge and if it is, return
        # the other major hexes it belongs to
        others = self.edge.get((q1, r1))
        if others:
            for qd, rd in others:
                yield q0 + qd, r0 + rd


###############################################################################
# HERE BE DRAGONS #############################################################
###############################################################################
# the rest of this is prototype code that is being preserved, albeit ##########
# commented out, in case there are concepts within that might be of use in ####
# the future. none of this is guaranteed to work if uncommented. ##############
###############################################################################

# def pixel_to_flat_hex(x, y, size):
#     q = ( 2/3 * x                ) / size
#     r = (-1/3 * x  +  sqrt3/3 * y) / size
#     return axial_round(q, r)


# def pixel_to_pointy_hex(x, y, size):
#     q = (sqrt3/3 * x  -  1/3 * y) / size
#     r = (                2/3 * y) / size
#     return axial_round(q, r)


# def flat_hex_to_pixel(q, r, size):
#     x = size * (    3/2 * q              )
#     y = size * (sqrt3/2 * q  +  sqrt3 * r)
#     return x, y


# def pointy_hex_to_pixel(q, r, size):
#     x = size * (sqrt3 * q  +  sqrt3/2 * r)
#     y = size * (                  3/2 * r)
#     return x, y


# def make_flat_hex(size):
#     h = sqrt3 * size
#     w = 2 * size
#     return (
#         ( w/2,    0),
#         ( w/4, -h/2),
#         (-w/4, -h/2),
#         (-w/2,    0),
#         (-w/4,  h/2),
#         ( w/4,  h/2))


# def make_pointy_hex(size):
#     h = 2 * size
#     w = sqrt3 * size
#     return (
#         ( w/2, -h/4),
#         (   0, -h/2),
#         (-w/2, -h/4),
#         (-w/2,  h/4),
#         (   0,  h/2),
#         ( w/2,  h/4))


# def hex_to_pixel(r, q, size, flat_or_pointy):
#     if flat_or_pointy:
#         return flat_hex_to_pixel(q, r, size)
#     else:
#         return pointy_hex_to_pixel(q, r, size)

# def draw_hex(draw, q, r, flat_or_pointy, size, x0, y0, **kwargs):
#     if flat_or_pointy:
#         x, y = flat_hex_to_pixel(q, r, size)
#         hex = make_flat_hex(size)

#     else:
#         x, y = pointy_hex_to_pixel(q, r, size)
#         hex = make_pointy_hex(size)

#     hex = tuple(
#         (x + x0 + xh, y + y0 + yh)
#         for xh, yh in hex)

#     draw.polygon(hex, **kwargs)


# def draw_hex_e(draw, q, r, shape, size, x0, y0, **kwargs):
#     x, y = hex_to_pixel(q, r, size, shape)
#     hex = make_hex_e(size, shape)


# def iter_mismatched_sub_hexes(radius):
#     if radius < 0:
#         return

#     #  0..radius           -radius
#     # -1..radius           -radius+1
#     # -2..radius           -radius+2
#     # ...
#     # -radius..radius      -radius+radius=0
#     for r0 in range(0, radius+1):
#         r = -radius + r0
#         for q in range(-r0, radius+1):
#             yield q, r

#     # -radius..radius-1    1
#     # -radius..radius-2    2
#     # -radius..radius-3    3
#     # ...
#     # -radius..0           radius
#     for r in range(1, radius+1):
#         for q in range(-radius, radius-r+1):
#             yield q, r


# def iter_matched_ring_hexes(radius):
#     if radius <= 1:
#         yield 0, 0
#         return

#     k = (radius - 1) // 3
#     min_k, max_k = -k, k
#     if radius % 3 == 1:
#         max_k = k - 1

#     qd, rd = 2, -1
#     q0, r0 = 0, -(radius - 1)

#     for k in range(min_k, max_k + 1):
#         q, r = q0 + k * qd, r0 + k * rd
#         yield q, r
#         for _ in range(6):
#             q, r = rotate(q, r)
#             yield q, r


# def iter_matched_sub_hexes(radius):
#     for r in range(1, radius+1):
#         yield from iter_matched_ring_hexes(r)
#         yield from iter_matched_edge_hexes(r)


# def draw_matched_minor_hexes(args, draw, center, q, r):
#     major_size = (2 * args.minor_radius - 1) * args.size
#     x, y = hex_to_pixel(r, q, major_size, not args.major_shape)

#     for q0, r0 in iter_matched_sub_hexes(args.minor_radius):
#         x0 = x + center[0]
#         y0 = y + center[1]
#         draw_hex(
#             draw, q0, r0, not args.major_shape, args.size, x0, y0,
#             outline=args.minor_color, fill=args.fill_color,
#             width=args.minor_width)


# def draw_matched_major_hex(args, draw, center, q, r):
#     # draw_matched_minor_hexes(args, draw, center, q, r)
#     major_size = (2 * args.minor_radius - 1) * args.size
#     draw_hex(
#         draw, q, r, not args.major_shape, major_size, *center,
#         outline=args.major_color, width=args.major_width)


# def draw_mismatched_minor_hexes(args, draw, center, q, r):
#     major_size = sqrt3 * args.size * args.minor_radius
#     x, y = hex_to_pixel(r, q, major_size, not args.major_shape)

#     for q0, r0 in iter_mismatched_sub_hexes(args.minor_radius):
#         x0 = x + center[0]
#         y0 = y + center[1]
#         draw_hex(
#             draw, q0, r0, args.major_shape, args.size, x0, y0,
#             outline=args.minor_color, fill=args.fill_color,
#             width=args.minor_width)


# def draw_mismatched_major_hex(args, draw, center, q, r):
#     major_size = sqrt3 * args.size * args.minor_radius
#     draw_hex(
#         draw, q, r, not args.major_shape, major_size, *center,
#         outline=args.major_color, width=args.major_width)


# def draw_major_hex_rings():

#     parser = ArgumentParser()
#     parser.add_argument(
#         '--dims', type=int, nargs=2, default=(1000, 1000),
#         help='width and length of image; default=%(default)s')
#     parser.add_argument(
#         '--size', type=int, default=25,
#         help='length of the side of a minor hex; default=%(default)s')
#     parser.add_argument(
#         '--major-shape', choices=('flat', 'pointy', 'same'),
#         default='pointy',
#         help='orientation of major hexes; default=%(default)s')
#     parser.add_argument(
#         '--minor-shape', choices=('flat', 'pointy', 'same'), default='same',
#         help='orientation of minor hexes')
#     parser.add_argument(
#         '--minor-radius', type=int, default=2,
#         help='''
#             number of minor hexes in radius of major hex;
#             default=%(default)s')
#     parser.add_argument(
#         '--major-radius', type=int, default=1,
#         help='; default=%(default)s')
#     parser.add_argument(
#         '--back-color', default='#000000ff')
#     parser.add_argument(
#         '--fill-color', default='#646464ff')
#     parser.add_argument(
#         '--minor-color', default='#ffffffff')
#     parser.add_argument(
#         '--major-color', default='#ff0000ff')
#     parser.add_argument(
#         '--minor-width', type=int, default=2)
#     parser.add_argument(
#         '--major-width', type=int, default=2)

#     args = parser.parse_args()

#     valid_shapes = True
#     if args.minor_shape == 'same':
#         args.minor_shape = args.major_shape
#     elif args.major_shape == 'same':
#         args.major_shape = args.minor_shape

#     if args.major_shape == 'same' and args.minor_shape == 'same':
#         sys.exit(
#             'At least one of the shapes must be explicitly specified, they '
#             'may not both be "same".')

#     flat_or_pointy = args.major_shape == 'pointy'
#     args.major_shape = args.major_shape == 'pointy'
#     args.minor_shape = args.minor_shape == 'pointy'

#     draw_major_hex = draw_matched_major_hex
#     draw_minor_hexes = draw_matched_minor_hexes
#     if args.major_shape != args.minor_shape:
#         draw_major_hex = draw_mismatched_major_hex
#         draw_minor_hexes = draw_mismatched_minor_hexes


#     with Image.new('RGBA', args.dims, color=args.back_color) as image:
#         draw = ImageDraw.Draw(image)
#         center = args.dims[0]//2, args.dims[1]//2

#         for q, r in iter_mismatched_sub_hexes(args.major_radius):
#             draw_minor_hexes(args, draw, center, q, r)

#         for q, r in iter_mismatched_sub_hexes(args.major_radius):
#             draw_major_hex(args, draw, center, q, r)
#         image.show()
