The excellent [guide to hexagonal grids](https://www.redblobgames.com/grids/hexagons/)
from [Amit Patel at Red Blob Games](https://www.redblobgames.com/) contains a
lot of information for working with grids of hexagons, but those familiar with
hexcrawls will notice that something is missing. Hexcrawls usually have a
two-layered grid: one is a finer scale grid, typically each hex having a scale
of a mile, under a larger grid with a scale of five or six miles.

Such grids have some interesting properties in themselves, it turns out, but
when I began this project, I was primarily interested in building tools to
assist with running hexcrawls, particularly, being able to determine the
coordinate of a major hex given the coordinate of a minor hex.

If we are given a pixel, we can [find the radial coordinate](https://www.redblobgames.com/grids/hexagons/#pixel-to-hex)
of both the major and minor hex that contain it. And because we can find the
[find center pixel](https://www.redblobgames.com/grids/hexagons/#hex-to-pixel)
of a hex, we can then find the major hex containing that pixel.

Looking at a traditional hexcrawl map, though, you might notice that this
procedure is not always straightforward as some minor hexes do not lie entirely
within a single hex, but are bisected on the boundaries between two major
hexes, colored green.

![matched radius 3](img/matched3.PNG)

But it gets worse: depending on the size of the major hex relative to the minor
hexes (refered to as the raidus of the major hex), different minor hexes may
lie on the boundary, and in some cases, might  actually be shared by three
major hexes rather than two, colored yellow. This behavior cycles, repeating
every three steps in radius.

![matched expanding radius](img/matched.gif)

If we were to instead orient the major hexes to be pointy with the minor hexes
flat, the behavior of the border hexes becomes better behaved: the minor hexes
at the corners of the major hexes are always shared by three major hexes, and
the outer ring is always solid and each minor hex is shared by two major hexes.
As the raidus of the major hex increases, the number of minor hexes on the
edges increases equally.

![mismatched expanding radius](img/mismatched.gif)


<!--
* Given a major hex, find all of the minor hexes that belong to it
* Does a minor hex belong to multiple major hexes


# Randomly Choosing a Minor Hex
## Standard 5-mile Hex
(Credit to `Filling in the Blanks`)

Roll D31

## Standard 5-mile Hex with Standard Dice
(Credit to a post on reddit, lost to time)

In a radius=3 matched grid, there will be 19 interior hexes and 12 bisected hexes
on the major hex's sides.

Roll D20:
  on 1-19, choose and interior hex;
  on 20, roll D12 to choose a bisected border hex.


# 6-mile Mismatched Hex
In a radius=3 mismatched grid, there will be 19 interior hexes, 12 bisected
hexes on the major hex's sides and 6 trisected hexes at each point.

Roll D20:
  on 1-19, choose an interior hex;
  on 20, continue.

Roll D6:
  on 1-4, roll D12 to choose a bisected border hex;
  on 5-6, roll D6 to choose a trisected border hex.

-->
