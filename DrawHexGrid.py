"""Draw hex grids."""

from hexes import HexShape
from hexes import MajorMinorHexGrid

from PIL import ImageFont, ImageDraw, Image

from argparse import ArgumentParser, Action, HelpFormatter
from itertools import product
from enum import Enum, auto
from sys import exit

import os


class struct:
    """Simple c-like struct."""

    def __init__(self, *args, **kwargs):
        """
        Construct the object.

        Parameters
        ----------
        *args - Initialize attributes with these names to None.
        **kwargs - Initialize the namedd attributes with the specified values.
        """
        for arg in args:
            self.__dict__[arg] = None
        self.__dict__.update(kwargs)


def int_or_float(x):
    """Parse and int or float from a string."""
    try:
        return int(x)
    except Exception:
        pass

    try:
        return float(x)
    except Exception:
        raise Exception(f'Could not parse value to int or float: {x}')


def none_or_str(x):
    """Parse None from a string, or return the string unchanged."""
    if x.lower() == 'none':
        return None
    return x


def count(it):
    """Count the number of items in an iterable."""
    return sum(1 for _ in it)


class CoordUnit(Enum):
    """Enumeration of coordinate types."""

    pixel = auto()
    relative = auto()
    minor = auto()
    major = auto()


class Coordinate:
    """Class to hold and translate between different coordinate types."""

    def __init__(self, c0, c1, unit):
        """Construct the object."""
        self.c0, self.c1 = c0, c1
        self.unit = unit

    def __str__(self):
        """Create a string representation of the coordinate."""
        if self.unit == CoordUnit.pixel:
            return f'<{self.c0},{self.c1}p>'
        if self.unit == CoordUnit.relative:
            return f'<{self.c0},{self.c1}r>'
        elif self.unit == CoordUnit.major:
            return f'<{self.c0},{self.c1}M>'
        elif self.unit == CoordUnit.minor:
            return f'<{self.c0},{self.c1}m>'
        else:
            assert 0

    def __repr__(self):
        """Return str(self)."""
        return str(self)

    @staticmethod
    def parse(x):
        """Parse a coordinate from a string."""
        unit = CoordUnit.pixel
        if x[-1] == 'p':
            x = x[:-1]
        elif x[-1] == 'r':
            unit = CoordUnit.relative
            x = x[:-1]
        elif x[-1] == 'M':
            unit = CoordUnit.major
            x = x[:-1]
        elif x[-1] == 'm':
            unit = CoordUnit.minor
            x = x[:-1]

        c0, c1, *rest = x.split(',')
        c0 = int_or_float(c0)
        c1 = int_or_float(c1)
        return Coordinate(c0, c1, unit)

    def pixel(self, grid):
        """Convert the coordinate to a pixel with respect to a hex grid."""
        if self.unit == CoordUnit.pixel:
            return self.c0, self.c1
        elif self.unit == CoordUnit.relative:
            return self.c0 + grid.x, self.c1 + grid.y
        elif self.unit == CoordUnit.major:
            return grid.major_pixel(self.c0, self.c1)
        elif self.unit == CoordUnit.minor:
            return grid.minor_pixel(self.c0, self.c1)
        else:
            assert 0

    def format_text(self, text, grid):
        """
        Convert to all possible coordinates with respect to a hex grid.

        Returns
        -------
        input
          c0, c1 - The coordinates that were given originally.

        pixel
          x, y - The absolute pixel coordinate.
          rx, ry - The pixel coordinate relative to the origin.

        major - The minor hex that contains the original coordinate.
          q, r, s - The cubic coordinates of the major hex.
          x, y - The pixel at the center of the major hex.

        minor - The minor hex that contains the original coordinate.
          q, r, s - The cubic coordinates of the minor hex.
          x, y - The pixel at the center of the minor hex.
        """
        input = struct(c0=self.c0, c1=self.c1)
        pixel = struct(*'xy', 'rx', 'ry')
        major = struct(*'qrsxy')
        minor = struct(*'qrsxy')

        if self.unit == CoordUnit.pixel:
            pixel.x, pixel.y = self.c0, self.c1
            pixel.rx, pixel.ry = pixel.x - grid.x, pixel.y - grid.y
            major.q, major.r = grid.major_hex(pixel.x, pixel.y)
            major.x, major.y = grid.major_pixel(major.q, major.r)
            minor.q, minor.r = grid.minor_hex(pixel.x, pixel.y)
            minor.x, minor.y = grid.minor_pixel(minor.q, minor.r)

        elif self.unit == CoordUnit.relative:
            pixel.rx, pixel.ry = self.c0, self.c1
            pixel.x, pixel.y = pixel.rx + grid.x, pixel.ry + grid.y
            major.q, major.r = grid.major_hex(pixel.x, pixel.y)
            major.x, major.y = grid.major_pixel(major.q, major.r)
            minor.q, minor.r = grid.minor_hex(pixel.x, pixel.y)
            minor.x, minor.y = grid.minor_pixel(minor.q, minor.r)

        elif self.unit == CoordUnit.major:
            major.q, major.r = self.c0, self.c1
            major.x, major.y = grid.major_pixel(major.q, major.r)
            minor.q, minor.r = grid.minor_hex(major.x, major.y)
            minor.x, minor.y = grid.minor_pixel(minor.q, minor.r)
            pixel.x, pixel.y = major.x, major.y
            pixel.rx, pixel.ry = pixel.x - grid.x, pixel.y - grid.y

        elif self.unit == CoordUnit.minor:
            minor.q, minor.r = self.c0, self.c1
            minor.x, minor.y = grid.minor_pixel(minor.q, minor.r)
            major.q, major.r = grid.major_hex(minor.x, minor.y)
            major.x, major.y = grid.major_pixel(major.q, major.r)
            pixel.x, pixel.y = minor.x, minor.y
            pixel.rx, pixel.ry = pixel.x - grid.x, pixel.y - grid.y

        minor.s = -(minor.q + minor.r)
        major.s = -(major.q + major.r)

        return text.format(input=input, pixel=pixel, major=major, minor=minor)


class PointContainer:
    """
    Point container to minimize effects of rounding in calculations.

    This container inserts a point only when it does not already contain
    another point within a certain distance from it.
    """

    def __init__(self, size, eps=0):
        """
        Initialize the container.

        Parameters
        ----------
        size - The length of the sides of the bins into which points will be
            categorized. Smaller bins reduce the number of points that must be
            examined.
        eps - The distance at which points are considered to be equal. This
            value should be selected to be larger than expected rounding
            errors, but smaller than the distance between any two points to be
            inserted into the container. Must be less than or equal to size.
        """
        assert eps <= size
        self.bins = {}
        self.size = size
        self.eps2 = eps ** 2

    def __call__(self, x, y):
        """
        Return a point close to x,y.

        Either return a point that has already been inserted that is within
        eps distance of x,y, or insert x,y and return it if one does not
        exist.
        """
        bins = self.bins
        size = self.size
        eps2 = self.eps2

        # calculate the bin the point belongs to
        bx, by = round(x / size), round(y / size)

        # examine the point's bin and neighboring bins for points within eps
        # distance of the input point
        # TODO: this could be optimized since we do not need to examine bins
        # whose edges are too far from the point being searched for
        for bx0, by0 in product(range(-1, 2), repeat=2):
            bin = bins.get((bx + bx0, by + by0))
            if not bin:
                continue

            for xy in bin:
                x0, y0 = xy
                if (x - x0) ** 2 + (y - y0) ** 2 <= eps2:
                    # this point is close to the input, return it
                    return xy

        # no points were nearby, add the new point and return it
        bin = bins.get((bx, by))
        if not bin:
            bin = []
            bins[bx, by] = bin

        bin.append((x, y))
        return bin[-1]


def draw_grid(draw, args, grid):
    """Draw the hex grid."""
    points = PointContainer(
        size=max(args.size / 2, 1),
        eps=max(args.size / 4, 1))

    draw.rectangle(args.box, fill=args.box_fill, width=args.box_width)

    for q, r in grid.major_grid(args.box):
        grid.draw_major(
            draw, q, r,
            smoothing=points,
            fill=args.major_fill,
            width=args.major_width)

    for q, r in grid.minor_grid(args.box):
        fill = args.minor_fill
        outline = args.minor_color

        if args.stylize_minor_hexes_by_count:
            n = count(grid.iter_major_hexes(q, r))
            if n == 2:
                fill = args.minor2_fill
                outline = args.minor2_color
            elif n == 3:
                fill = args.minor3_fill
                outline = args.minor3_color
            else:
                # do nothing, we defaulted to the correct colors
                pass

        grid.draw_minor(
            draw, q, r,
            smoothing=points,
            fill=fill,
            outline=outline,
            width=args.minor_width)

    for q, r in grid.major_grid(args.box):
        grid.draw_major(
            draw, q, r,
            smoothing=points,
            outline=args.major_color,
            width=args.major_width)

    draw.rectangle(args.box, outline=args.box_color, width=args.box_width)


def draw_text(draw, args, grid):
    """Draw text using coordinates with respect to a hex grid."""
    current_font = None
    current_size = None
    current_color = None
    loaded_fonts = {}

    for arg in args.text:
        if arg.name == 'font':
            current_font = arg.params[0]
            font = None

        elif arg.name == 'size':
            current_size = arg.params[0]
            font = None

        elif arg.name == 'color':
            current_color = arg.params[0]

        elif arg.name == 'text':
            coord, text = arg.params
            xy = coord.pixel(grid)
            text = coord.format_text(text, grid)

            font = loaded_fonts.get((current_font, current_size))
            if not font:
                if current_font is None:
                    font = ImageFont.load_default()
                elif current_font.lower().endswith('.ttf'):
                    font = ImageFont.truetype(current_font, current_size)
                loaded_fonts[current_font, current_size] = font

            draw.text(
                xy,
                text,
                font=font,
                anchor='mm',
                align='center',
                fill=current_color)


class Command:
    """A named command and its parameters."""

    def __init__(self, name, params):
        """Construct the object."""
        self.name = name
        self.params = params

    def __str__(self):
        """Return a string representation with the name and parameters."""
        return f'<{self.name}: {self.params}>'

    def __repr__(self):
        """Return str(self)."""
        return str(self)


class CommandBuilder:
    """
    Builds commands from arguments from the command line.

    An instance of a CommandBuilder is passed as the type parameter in
    conjunction with a CommandChainAction to add_argument().
    """

    def __init__(self, name, *types):
        """Construct the object."""
        self.name = name
        self.types = types

    def __call__(self, value):
        """Just return the argument unchanged."""
        return value

    def make(self, *values):
        """Make the Command object, casting argument to the correct types."""
        n = min(len(values), len(self.types))
        values = [t(v) for v, t in zip(values, self.types)] + [*values[n:]]
        return Command(self.name, values)


class CommandChainAction(Action):
    """Action that constructs chains of commands."""

    def __init__(self, *args, **kwargs):
        """Construct the Action and perform it on the default value."""
        namespace = kwargs.get('namespace')
        if namespace is not None:
            del kwargs['namespace']

        assert 'type' in kwargs
        assert isinstance(kwargs['type'], CommandBuilder)
        super().__init__(*args, **kwargs)

        if self.default is not None:
            self(None, namespace, self.default, None)

    def __call__(self, parser, namespace, values, option_string=None):
        """Add values to the namespace."""
        if self.nargs is None:
            values = [values]

        all_values = getattr(namespace, self.dest, [])
        if not all_values:
            setattr(namespace, self.dest, all_values := [])

        all_values.append(self.type.make(*values))


class ParagraphFormatter(HelpFormatter):
    """Format messages as paragraphs with empty lines."""

    def __split_paragraphs(self, text):
        """
        Split multine strings into paragraphs.

        Paragraphs are collected by grouping lines that are not separated by
        blank lines themselves. A single blank line will be converted to a
        paragraph break and multiple blank lines to a single blank line.
        """
        lines = [line.strip() for line in text.split('\n')]

        while not lines[0]:
            lines.pop(0)
        while not lines[-1]:
            lines.pop()

        paragraphs = [0]
        for line in lines:
            if isinstance(paragraphs[-1], int):
                if line:
                    paragraphs.append(line)
                else:
                    paragraphs[-1] += 1

            else:
                if line:
                    paragraphs[-1] += ' ' + line
                else:
                    paragraphs.append(0)

        return [
            '\n' if isinstance(paragraph, int) else paragraph
            for paragraph in paragraphs
            if paragraph]

        return paragraphs

    def _split_lines(self, text, width):
        """Format argument help messages."""
        lines = []
        for paragraph in self.__split_paragraphs(text):
            lines.extend(super()._split_lines(paragraph, width))

        return lines

    def _fill_text(self, text, width, indent):
        """Format program description and epilog messages."""
        # NOTE: whatever calls _fill_text seems to truncate multiple
        # consecutive new-lines to at most two, so you can put a blank line
        # between lines of text, but not two or more.
        chunks = []
        for paragraph in self.__split_paragraphs(text):
            if paragraph.startswith('\n'):
                chunks.append(paragraph)
            else:
                chunks.append(super()._fill_text(paragraph, width, indent))

        return '\n'.join(chunks)


def parse_command_line():
    """Parse the command line."""
    parser = ArgumentParser(
        formatter_class=ParagraphFormatter,
        description='''
            Draw a hex grid with major and minor hexes.


            The parts of the grid in this order:

            * background\n
            * box fill\n
            * major fill\n
            * minor fill\n
            * minor edges\n
            * major edges\n
            * box edges\n
            * text\n

            Because a single layer is used, transparency may not work as
            expected with pixels replacing each other as they are drawn, rather
            than being applied on top of each other. For example, drawing a
            semi-transparent red pixel on top of a blue pixel results in a
            semi-transparent red pixel, not a purple pixel.

            With the exception of the base background color, specifying 'none'
            for any color will result in
            ''')
    args = struct()

    parser.add_argument(
        '--dims', type=int, nargs=2, default=(1000, 1000),
        help='Width and length of image; default=%(default)s')
    parser.add_argument(
        '--box', type=int, nargs=4, default=None,
        help='''
            Only draw hexes which overlap with this region of the image,
            sepcified by min-x min-y max-x max-y;
            defaults to whole image
            ''')
    parser.add_argument(
        '--origin', type=int_or_float, nargs=2, default=None,
        help='''
            Pixel to use as the center of the central hex;
            defaults to box center
            ''')
    parser.add_argument(
        '--size', type=int, default=25,
        help='Pixel length of the side of a minor hex; default=%(default)s')
    parser.add_argument(
        '--major-shape', choices=('flat', 'pointy', 'same'), default='pointy',
        help='Orientation of major hexes; default=%(default)s')
    parser.add_argument(
        '--minor-shape', choices=('flat', 'pointy', 'same'), default='same',
        help='Orientation of minor hexes')
    parser.add_argument(
        '--radius', type=int, default=3,
        help='''
            Radius of major hexes in terms of minor hexes; default=%(default)s
            ''')

    parser.add_argument(
        '--back-color', type=none_or_str, default='#000000ff',
        help='Color of the background of the image; default=%(default)s')

    parser.add_argument(
        '--major-width', type=int, default=1,
        help='Width of sides of major hexes; default=%(default)s')
    parser.add_argument(
        '--major-color', type=none_or_str, default='#ff0000ff',
        help='Color of sides of major hexes; default=%(default)s')
    parser.add_argument(
        '--major-fill', type=none_or_str, default=None,
        help='Color of interiors of major hexes; default=%(default)s')

    parser.add_argument(
        '--minor-width', type=int, default=1,
        help='Width of the sides of minor hexes; default=%(default)s')
    parser.add_argument(
        '--minor-color', type=none_or_str, default='#ffffffff',
        help='Color of the sides of minor hexes; default=%(default)s')
    parser.add_argument(
        '--minor-fill', type=none_or_str, default=None,
        help='Color of the interiors of minor hexes; default=%(default)s')

    parser.add_argument(
        '--minor2-color', type=none_or_str, default=None,
        help='''
            Color of the sides of minor hexes shared by two major hexes;
            default=%(default)s
            ''')
    parser.add_argument(
        '--minor2-fill', type=none_or_str, default=None,
        help='''
            Color of the interiors of minor hexes shared by two major hexes;
            default=%(default)s
            ''')

    parser.add_argument(
        '--minor3-color', type=none_or_str, default=None,
        help='''
            Color of the sides of minor hexes shared by three major hexes;
            default=%(default)s
            ''')
    parser.add_argument(
        '--minor3-fill', type=none_or_str, default=None,
        help='''
            Color of the interiors of minor hexes shared by three major hexes;
            default=%(default)s
            ''')

    parser.add_argument(
        '--box-width', type=int, default=0,
        help='Width of the sides of the box; default=%(default)s')
    parser.add_argument(
        '--box-color', type=none_or_str, default=None,
        help='Color of the sides of the box; default=%(default)s')
    parser.add_argument(
        '--box-fill', type=none_or_str, default=None,
        help='Color of the interior of the box; default=%(default)s')

    parser.add_argument(
        '--palette', choices=('demo', 'overlay', 'dark-overlay', 'paper'),
        help='''
            Predefined color and width selections. Note that all other color
            and width parameters for boxes and hexes are ignored when using
            this option.

            * overlay - Draw the grid to layer onto an existing map with
            slightly transparent gray and black major and minor hex edges on
            completely transparent background.

            * dark-overlay - A non-transparent version of 'overlay' putting
            more emphasis on the grid.

            * paper - Draw the grid with gray major and blue minor hexes on a
            white background.

            * demo - Show how the major hex radius affects which minor hexes
            are on its edge: minor hexes completely contained within a single
            major hex are colored blue, those on the boundary between two major
            hexes are colored green; and those on the boundary between three
            major hexes are colored yellow.
            ''')

    parser.add_argument(
        '--text', nargs=2, dest='text',
        action=CommandChainAction,
        type=CommandBuilder('text', Coordinate.parse, str),
        help='''
            At a specified coordinate, draw a text string with the current font
            settings.


            Coordinates are of the form x,yu where x and y are the coordinate
            and u is the unit:

            p or unspecified - The coordinate is a pixel.

            r - The coordinate is a pixel relative to --origin.

            M - The coordinate is the axial coordinate of a major hex, relative
            to the major hex containing --origin.

            m - The coordinate is the axial coordinate of a minor hex, relative
            to the minor hex containing --origin.
            ''')

    # choose a default font from a list of known font files, choose the first
    # that actually exists
    default_fonts = (
        '/usr/share/fonts/chromeos/monotype/arialnb.ttf',
        '/usr/share/fonts/truetype/msttcorefonts/arial.ttf',)

    default_fonts = tuple(
        path for path in default_fonts
        if os.path.exists(path))

    default_font = None
    if default_fonts:
        default_font = default_fonts[0]

    parser.add_argument(
        '--text-font', dest='text',
        action=CommandChainAction,
        type=CommandBuilder('font', str),
        default=default_font,
        namespace=args,
        help='Set the font for subsequent --text; default=%(default)s')

    parser.add_argument(
        '--text-color', dest='text',
        action=CommandChainAction,
        type=CommandBuilder('color', none_or_str),
        default='#ffffffff',
        namespace=args,
        help='Set the color for subsequent --text; default=%(default)s')

    parser.add_argument(
        '--text-size', dest='text',
        action=CommandChainAction,
        type=CommandBuilder('size', int),
        default=24,
        namespace=args,
        help='Set the point size for subsequent --text; default=%(default)s')

    parser.parse_args(namespace=args)

    if args.palette == 'demo':
        args.back_color =   '#000000ff'     # noqa: E222
        args.minor_color =  '#ffffffff'     # noqa: E222
        args.minor2_color = '#ffffffff'     # noqa: E222
        args.minor3_color = '#ffffffff'     # noqa: E222
        args.major_color =  '#404040ff'     # noqa: E222
        args.box_color =    None            # noqa: E222

        args.minor_fill =   None            # noqa: E222
        args.minor2_fill =  '#008000ff'     # noqa: E222
        args.minor3_fill =  '#808000ff'     # noqa: E222
        args.major_fill =   '#000040ff'     # noqa: E222
        args.box_fill =     None            # noqa: E222

        args.minor_width =  3               # noqa: E222
        args.minor2_width = 3               # noqa: E222
        args.minor3_width = 3               # noqa: E222
        args.major_width =  5               # noqa: E222
        args.box_width =    0               # noqa: E222

    elif args.palette == 'overlay':
        args.back_color =   '#00000000'     # noqa: E222
        args.minor_color =  '#000000a0'     # noqa: E222
        args.minor2_color = None            # noqa: E222
        args.minor3_color = None            # noqa: E222
        args.major_color =  '#404040a0'     # noqa: E222
        args.box_color =    None            # noqa: E222

        args.minor_fill =   None            # noqa: E222
        args.minor2_fill =  None            # noqa: E222
        args.minor3_fill =  None            # noqa: E222
        args.major_fill =   None            # noqa: E222
        args.box_fill =     None            # noqa: E222

        args.minor_width =  3               # noqa: E222
        args.minor2_width = 0               # noqa: E222
        args.minor3_width = 0               # noqa: E222
        args.major_width =  5               # noqa: E222
        args.box_width =    0               # noqa: E222

    elif args.palette == 'dark-overlay':
        args.back_color =   '#00000000'     # noqa: E222
        args.minor_color =  '#000000ff'     # noqa: E222
        args.minor2_color = None            # noqa: E222
        args.minor3_color = None            # noqa: E222
        args.major_color =  '#404040ff'     # noqa: E222
        args.box_color =    None            # noqa: E222

        args.minor_fill =   None            # noqa: E222
        args.minor2_fill =  None            # noqa: E222
        args.minor3_fill =  None            # noqa: E222
        args.major_fill =   None            # noqa: E222
        args.box_fill =     None            # noqa: E222

        args.minor_width =  2               # noqa: E222
        args.minor2_width = 0               # noqa: E222
        args.minor3_width = 0               # noqa: E222
        args.major_width =  3               # noqa: E222
        args.box_width =    0               # noqa: E222

    elif args.palette == 'paper':
        args.back_color =   '#ffffffff'     # noqa: E222
        args.minor_color =  '#6060b0ff'     # noqa: E222
        args.minor2_color = None            # noqa: E222
        args.minor3_color = None            # noqa: E222
        args.major_color =  '#808080ff'     # noqa: E222
        args.box_color =    None            # noqa: E222

        args.minor_fill =   None            # noqa: E222
        args.minor2_fill =  None            # noqa: E222
        args.minor3_fill =  None            # noqa: E222
        args.major_fill =   None            # noqa: E222
        args.box_fill =     None            # noqa: E222

        args.minor_width =  1               # noqa: E222
        args.minor2_width = 0               # noqa: E222
        args.minor3_width = 0               # noqa: E222
        args.major_width =  1               # noqa: E222
        args.box_width =    0               # noqa: E222

    if not args.box:
        args.box = 0, 0, args.dims[0] - 1, args.dims[1] - 1

    if not args.origin:
        args.origin = (
            (args.box[0] + args.box[2]) / 2,
            (args.box[1] + args.box[3]) / 2)

    args.minor_shape = HexShape.parse(args.minor_shape)
    args.major_shape = HexShape.parse(args.major_shape)

    if args.minor_shape == HexShape.same:
        args.minor_shape = args.major_shape
    elif args.major_shape == HexShape.same:
        args.major_shape = args.minor_shape

    if args.major_shape == args.minor_shape == HexShape.same:
        exit(
            'At least one of the shapes must be explicitly specified, they '
            'may not both be "same".')

    if args.major_shape != args.minor_shape and args.radius < 1:
        exit('Mismatched hex shapes cannot have radius 0.')

    args.stylize_minor_hexes_by_count = (
        args.minor2_color or args.minor2_fill
        or args.minor3_color or args.minor3_fill)

    if args.stylize_minor_hexes_by_count:
        if not args.minor2_color:
            args.minor2_color = args.minor_color
        if not args.minor2_fill:
            args.minor2_fill = args.minor_fill
        if not args.minor3_color:
            args.minor3_color = args.minor_color
        if not args.minor3_fill:
            args.minor3_fill = args.minor_fill

    return args


def main():
    """Draw hex grids."""
    args = parse_command_line()

    grid = MajorMinorHexGrid(
        *args.origin,
        args.size,
        args.radius,
        args.major_shape,
        args.minor_shape)

    with Image.new('RGBA', args.dims, color=args.back_color) as image:
        draw = ImageDraw.Draw(image)
        draw_grid(draw, args, grid)
        draw_text(draw, args, grid)
        image.show()


if __name__ == '__main__':
    main()
